//
//  ViewController.swift
//  table view e collection view
//
//  Created by Alessandro Barruffo on 10/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//

import UIKit
import NotificationCenter
import AVKit


class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate{
   
    
    
    
        
    var movieURL : URL?
    
    
    
    @IBOutlet weak var segmentedControl: SegmentedController!
    
    @IBOutlet weak var tableView: UITableView!
    
    
      override func viewDidLoad() {
          
          super.viewDidLoad()
          
          tableView.dataSource = self
          tableView.delegate = self
          
          
          
          let name = Notification.Name("buttonTapped")
          
          NotificationCenter.default.addObserver(self, selector:
              #selector(displayPlayer), name: name, object: movieURL)
          
      
          let fontAttributes = [NSAttributedString.Key.font: UIFont(name: "PT Sans Caption", size: 12.0)!]
          UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
      
          
          // Do any additional setup after loading the view.
      }
      
      
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        
        return .lightContent
    }
    
      
      
      @objc func displayPlayer(notification : Notification) {
          
          
          
          let playerViewController = self.storyboard?.instantiateViewController(identifier: "PlayerViewController") as! AVPlayerViewController
          
          
       let link = notification.object as! String
          
          
          let url = URL(string: link)
          
        
          
          
          playerViewController.player = AVPlayer(url : url!)
          
          playerViewController.player?.play()
          
          self.present(playerViewController, animated: true, completion: nil)
          
          
          
      }
      

       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             return 6
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          
      
          
          if segmentedControl.selectedSegmentIndex == 1 {
              
              
                    if indexPath.row == 0 {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTableCell") as! FirstTableViewCell
                        
                        return cell
                        
                        
                    }else {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as! TableViewCell
                      
                      
                        
                         cell.indexPathForCell = indexPath
                      
                cell.segmentIndex = segmentedControl.selectedSegmentIndex
                     

                      

                        if indexPath.row  == 1 {
                            
                            cell.labelRow.text = "Top movies"
                            
                    
                        } else if indexPath.row == 2 {
                            cell.labelRow.text = "Comedy movies"

                            
                        }else if indexPath.row == 3 {
                            
                            cell.labelRow.text = "Drama movies"
                            
                        }else if indexPath.row == 4 {
                            
                            cell.labelRow.text = "Thriller movies"
                            
                        }else if indexPath.row == 5 {
                            
                            cell.labelRow.text = "Action and adventure movies"
                            
                        }
                        
                        return cell
                        
                    }
              
              
              
              
          }
          else if segmentedControl.selectedSegmentIndex == 0 {
              
              
        if indexPath.row == 0 {
                                  
            let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTableCell") as! FirstTableViewCell
                                  
            return cell
                                  
                                  
            }else{
                                  
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as! TableViewCell
                                  
            cell.indexPathForCell = indexPath
                  
            cell.segmentIndex = segmentedControl.selectedSegmentIndex
                  
                  
                if indexPath.row  == 1 {
                                      
                    cell.labelRow.text = "Top TV"
                                      
                              
                    } else if indexPath.row == 2 {
                                      
                    cell.labelRow.text = "Comedy TV"

                                      
                    }else if indexPath.row == 3 {
                                      
                    cell.labelRow.text = "Drama TV"
                                      
                    }else if indexPath.row == 4 {
                                      
                    cell.labelRow.text = "Thriller TV"
                                      
                    }else if indexPath.row == 5 {
                                      
                    cell.labelRow.text = "Action and adventure TV"
                                      
                    }
                                  
                    return cell
                                  
                    }
              
           }else {
            
              
              if indexPath.row == 0 {
                                            
              let cell = tableView.dequeueReusableCell(withIdentifier: "FirstTableCell") as! FirstTableViewCell
                                            
                
               cell.segmentIndex = segmentedControl.selectedSegmentIndex
            
                
                
             return cell
                                            
                                            
            }else {
                                            
                  let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as! TableViewCell
                                            
                  cell.indexPathForCell = indexPath
                 
                  cell.segmentIndex = segmentedControl.selectedSegmentIndex
                  
                    
                 
                  if indexPath.row  == 1 {
                                                
                    cell.labelRow.text = " Kids and family TV"
                                                
                                        
                  } else if indexPath.row == 2 {
                  
                      cell.labelRow.text = "Kids and family movies"

                                                
                  }else if indexPath.row == 3 {
                                                
                  cell.labelRow.text = "Animated movies"
                                                
                  }else if indexPath.row == 4 {
                                                
                  cell.labelRow.text = "Educational TV"
                                                
                  }else if indexPath.row == 5 {
                                                
                  cell.labelRow.text = "TV and movies for little kids"
                                                
                  }
                                            
                  return cell
          }
          
          
        }
          
      }
      
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
              
              
              if indexPath.row == 0 {
                  
                  
                  return 195.0
              }
              
              
              else {
                  
                  return  155.0
                  
              }
              
              
          }
      
   
    @IBAction func segValueChanged(_ sender: SegmentedController) {
        
        
        let name = Notification.Name("segmentedControlChanged")
                    
       NotificationCenter.default.post(name: name, object: segmentedControl.selectedSegmentIndex)
                    
    
                    
        
     UIView.transition(with: view, duration: 0.2,options: .transitionCrossDissolve, animations: {
            
            
            self.view.isHidden = false
            
            
        
        }, completion: nil)
        
        UIView.transition(with: view, duration: 0.2,options: .transitionCrossDissolve, animations: {
            
        

            self.view.isHidden = false
            
        }, completion: nil)
        
        

                if sender.selectedSegmentIndex == 0 {
                              
                           
                           
                    tableView.reloadData()
                           
                         
                             
                }
                else if sender.selectedSegmentIndex == 1{
                           
                             tableView.reloadData()
                           
                     
                             
                          }else {
                              tableView.reloadData()
                           
                             
                          }
               
        
        
        
    }
    
  
    
    
    
    
    
    
}

