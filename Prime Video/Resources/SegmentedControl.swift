//
//  SegmentedControl.swift
//  table view e collection view
//
//  Created by Alessandro Barruffo on 10/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//
import Foundation
import UIKit


@IBDesignable
class SegmentedController: UIControl {
    
    var buttons = [UIButton]()
    var selector : UIView!
    var selectedSegmentIndex = 0
    
    
    @IBInspectable
    var selectorColor : UIColor = .white {
        
        didSet{
            
            updateView()
        }
    
     }
    
    @IBInspectable
    var borderWidth : CGFloat = 0 {
        
        didSet{
            
            layer.borderWidth = borderWidth
        }
        
        
    }
    
    
    @IBInspectable
    var borderColor : UIColor = UIColor.clear {
        
        didSet{
            
             layer.borderColor = borderColor.cgColor
        }
        
    
        
    }
 

    @IBInspectable
    var textColor : UIColor = UIColor.lightGray{
        
        didSet{
            
            updateView()
        }
    }
    
    @IBInspectable
    var commaSeparatedButtonTitles : String = "" {
        
        didSet{
            
            updateView()
        }
        
    }
    
    
    func updateView() {
        
        
        
        buttons.removeAll()
        
        subviews.forEach{ (view) in
            view.removeFromSuperview()
            }
             
        
        let buttonTitles = commaSeparatedButtonTitles.components(separatedBy: ",")
        
    
        
        for buttonTitle in buttonTitles {
            
            let button = UIButton(type : .system)
            
            

            button.setTitle(buttonTitle, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonTapped(button:) ), for: .touchUpInside )
            buttons.append(button)
            
            
        }
        
        selector = UIView(frame: CGRect(x: 22 ,y:50,width: CGFloat(35),height: 5.4))
       
        
       
        selector.backgroundColor = selectorColor
        addSubview(selector)
        
        
        
        let sv = UIStackView(arrangedSubviews: buttons)
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillProportionally
        addSubview(sv)
        
        //with the following code I am setting the width anf the eight of the stack view wqual to the uiview that represent the segmented controller
        sv.translatesAutoresizingMaskIntoConstraints = false //beacuse I want to set my own constraints
        sv.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        sv.bottomAnchor.constraint(equalTo : self.bottomAnchor).isActive = true
        sv.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        sv.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
    

    }
    

    override func draw(_ rect: CGRect) {
        
        
    }
    
    
    
    @objc func buttonTapped(button : UIButton){
        
        for (buttonIndex,btn) in buttons.enumerated() {
            
            if btn == button {
            
            selectedSegmentIndex = buttonIndex
                
                UIView.animate(withDuration: 0.3, animations: {
        
                    
                    if buttonIndex == 0{
                        
                      self.selector.frame.origin.x = 22
                                             
                      self.selector.frame.size.width = CGFloat(35)
                    
                    }else if buttonIndex == 1{
                        
                        self.selector.frame.origin.x = 132
                                              
                        self.selector.frame.size.width = CGFloat(62)
                        
                    }else if buttonIndex == 2 {
                        
                        self.selector.frame.origin.x = 267
                        
                        self.selector.frame.size.width = CGFloat(45)
                    }
                })
                
            }
            
            
        }
        
        sendActions(for: .valueChanged)
    }
    

}

