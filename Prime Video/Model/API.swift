//
//  API.swift
//  Prime Video
//
//  Created by Alessandro Barruffo on 12/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//

import Foundation


class API {
    
    
    private static let singleton = API()
    
    
   var topMovies : [Movie] = [ Movie(image : #imageLiteral(resourceName: "Image0"), url: "https://lnx.farmaciaeuropa.it/ada/video7.mov"), Movie(image :#imageLiteral(resourceName: "Image1") ,url : "https://lnx.farmaciaeuropa.it/ada/video2.mov"), Movie(image: #imageLiteral(resourceName: "Image2"), url: "https://lnx.farmaciaeuropa.it/ada/video3.mov") ]
    
 
   var comedyMovies : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image12"), url: "https://lnx.farmaciaeuropa.it/ada/video12.mov"),Movie (image: #imageLiteral(resourceName: "Image13"), url: "https://movietrailers.apple.com/movies/independent/after-class/after-class-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image14"), url: "https://movietrailers.apple.com/movies/independent/jay-and-silent-bob-reboot/jay-and-silent-bob-reboot-trailer-1_h480p.mov")]
    
    
   var dramaMovies : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image0"), url: "https://lnx.farmaciaeuropa.it/ada/video7.mov"),Movie(image: #imageLiteral(resourceName: "Image3"), url: "https://lnx.farmaciaeuropa.it/ada/video4.mov"),Movie(image: #imageLiteral(resourceName: "Image15"), url: "https://lnx.farmaciaeuropa.it/ada/video6.mov")]
    
    
   var thrillerMovies : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image4"), url: "https://lnx.farmaciaeuropa.it/ada/video5.mov"), Movie(image: #imageLiteral(resourceName: "Image2"), url: "https://lnx.farmaciaeuropa.it/ada/video3.mov"), Movie(image: #imageLiteral(resourceName: "Image16"), url: "https://lnx.farmaciaeuropa.it/ada/video11.mov")]
    
   var actionAndAdventureMovies : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image17"), url: "https://lnx.farmaciaeuropa.it/ada/video14.mov"), Movie(image: #imageLiteral(resourceName: "Image1"), url: "https://lnx.farmaciaeuropa.it/ada/video2.mov"),Movie(image: #imageLiteral(resourceName: "Image18"), url: "https://lnx.farmaciaeuropa.it/ada/video18.mov")]
    
    
    var topTV : [Movie]  = [Movie(image: #imageLiteral(resourceName: "Image22"), url: "http://movietrailers.apple.com/movies/sony/a-beautiful-day-in-the-neighborhood/a-beautiful-day-in-the-neighborhood-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image23"), url: "https://movietrailers.apple.com/movies/independent/21-bridges/21-bridges-trailer-3_h480p.mov"),Movie(image:#imageLiteral(resourceName: "Image24"), url: "http://movietrailers.apple.com/movies/focus_features/dark-waters/dark-waters-trailer-1_h480p.mov")]
    
    var comedyTV : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image12"), url: "https://lnx.farmaciaeuropa.it/ada/video12.mov"),Movie (image: #imageLiteral(resourceName: "Image13"), url: "https://movietrailers.apple.com/movies/independent/after-class/after-class-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image14"), url: "https://movietrailers.apple.com/movies/independent/jay-and-silent-bob-reboot/jay-and-silent-bob-reboot-trailer-1_h480p.mov")]
    
    var dramaTV : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image0"), url: "https://lnx.farmaciaeuropa.it/ada/video7.mov"),Movie(image: #imageLiteral(resourceName: "Image3"), url: "https://lnx.farmaciaeuropa.it/ada/video4.mov"),Movie(image: #imageLiteral(resourceName: "Image15"), url: "https://lnx.farmaciaeuropa.it/ada/video6.mov")]
    
    var thrillerTV : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image4"), url: "https://lnx.farmaciaeuropa.it/ada/video5.mov"), Movie(image: #imageLiteral(resourceName: "Image2"), url: "https://lnx.farmaciaeuropa.it/ada/video3.mov"), Movie(image: #imageLiteral(resourceName: "Image16"), url: "https://lnx.farmaciaeuropa.it/ada/video11.mov")]
    
    var actionAndAdventureTV : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image17"), url: "https://lnx.farmaciaeuropa.it/ada/video14.mov"), Movie(image: #imageLiteral(resourceName: "Image1"), url: "https://lnx.farmaciaeuropa.it/ada/video2.mov"),Movie(image: #imageLiteral(resourceName: "Image18"), url: "https://lnx.farmaciaeuropa.it/ada/video18.mov")]
    
    var kidsAndFamilyTV :[Movie] = [Movie(image: #imageLiteral(resourceName: "Image10"), url: "https://movietrailers.apple.com/movies/disney/mulan/mulan-trailer-2_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image11"), url: "https://movietrailers.apple.com/movies/sony/jumanji-the-next-level/jumanji-the-next-level-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image9"), url: "http://movietrailers.apple.com/movies/disney/jungle-cruise/jungle-cruise-trailer-1_h480p.mov")]
    
    var kidsAndFamilyMovies : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image10"), url: "https://movietrailers.apple.com/movies/disney/mulan/mulan-trailer-2_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image11"), url: "https://movietrailers.apple.com/movies/sony/jumanji-the-next-level/jumanji-the-next-level-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image9"), url: "http://movietrailers.apple.com/movies/disney/jungle-cruise/jungle-cruise-trailer-1_h480p.mov")]
    
    var animatedMovies : [Movie] = [Movie(image: #imageLiteral(resourceName: "Image5"), url: "https://movietrailers.apple.com/movies/independent/arctic-dogs/arctic-dogs-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image6"), url: "https://movietrailers.apple.com/movies/universal/trolls-world-tour/trolls-world-tour-trailer-2_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image7"), url: "https://movietrailers.apple.com/movies/paramount/the-spongebob-movie-sponge-on-the-run/the-spongebob-movie-sponge-on-the-run-trailer-1_h480p.mov")]

    
    var tvAndMoviesForLittleKids :[Movie] = [Movie(image: #imageLiteral(resourceName: "Image8"), url: "http://movietrailers.apple.com/movies/disney/onward/onward-trailer-2-usca_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image20"), url: "http://movietrailers.apple.com/movies/independent/racetime/racetime-trailer-1_h480p.mov"),Movie(image: #imageLiteral(resourceName: "Image21"), url: "http://movietrailers.apple.com/movies/fox/spies-in-disguise/spies-in-disguise-trailer-2_h480p.mov")]
    
    var educationalTV : [Movie] = []
    
    
    
    
    private init(){
        

    }
    
    
    static func getApi() -> API {
        
        
        return singleton
    }
    
    func getMovies() -> [Movie]{
        
        return topMovies
    }
    
    func getTotalRows() -> Int {
        return 30
    }
    
    
 
}
