//
//  Movie.swift
//  Prime Video
//
//  Created by Alessandro Barruffo on 12/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//

import Foundation
import UIKit



class Movie : NSObject {
    

    var  image : UIImage?
    var  url : String?
    
    init(image : UIImage, url : String){
        
        
        self.image = image
        self.url = url
        
    }
      
    
    func getImageName() -> UIImage {
        
        return image!
    }
    
    
    func getUrl() -> String {
        
        return url!
        
    }
    
    
    
    
}
