//
//  TableViewCell.swift
//  table view e collection view
//
//  Created by Alessandro Barruffo on 10/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell, UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
    
    
    

    var movieURL : String? 
    
    
    var indexPathForCell: IndexPath?  //indexPath della cella della table view

    @IBOutlet weak var labelRow: UILabel!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var segmentIndex  : Int?
       
    
    
     override func awakeFromNib() {
            
            
            
            let nameSegControlChanged = Notification.Name("segmentedControlChanged")
                 
                NotificationCenter.default.addObserver(self, selector: #selector(collectionCellReloadData), name: nameSegControlChanged, object: segmentIndex)
            
            super.awakeFromNib()
            
            collectionView.dataSource = self
            collectionView.delegate = self


        }
        
        @objc func collectionCellReloadData(notification : Notification) {
              
              
              
            segmentIndex = notification.object as! Int?
            
            collectionView.reloadData()
            
            
              
              
              
          }
        
        
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return API.getApi().topMovies.count
            
             }
             
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
        
           
            if self.segmentIndex == 1 {
                
          
                
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell
                    
                    let index = indexPathForCell!.row
                
            
                          
                    cell.movieImageView.layer.cornerRadius = 8
                          
                    if index == 1 {
                            
                          cell.movieImageView.image = API.getApi().topMovies[indexPath.row].image
                              
                          cell.movieURL = API.getApi().topMovies[indexPath.row].url
                        }
                    else if index == 2 {
                              
                              cell.movieImageView.image = API.getApi().comedyMovies[indexPath.row].image
                
                              cell.movieURL = API.getApi().comedyMovies[indexPath.row].url
                              
                    }else if index == 3 {
                              
                              cell.movieImageView.image = API.getApi().dramaMovies[indexPath.row].image
                                             
                              cell.movieURL = API.getApi().dramaMovies[indexPath.row].url
                                         
                    }else if index == 4 {
                              
                              
                              cell.movieImageView.image = API.getApi().thrillerMovies[indexPath.row].image
                                             
                              cell.movieURL = API.getApi().thrillerMovies[indexPath.row].url
                              
                          
                    }else if index == 5 {
                              
                              cell.movieImageView.image = API.getApi().actionAndAdventureMovies   [indexPath.row].image
                                             
                              cell.movieURL = API.getApi().actionAndAdventureMovies[indexPath.row].url
                              
                              
                          }
                          
                return cell
                
                
            }else if self.segmentIndex == 0 {
                
          
                
                
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell
                    
                    let index = indexPathForCell!.row
                          
                        
                          cell.movieImageView.layer.cornerRadius = 8
                          
                          if index == 1 {
                              
                              
                          cell.movieImageView.image = API.getApi().topTV[indexPath.row].image
                              
                          cell.movieURL = API.getApi().topTV[indexPath.row].url
                              
                        }
                          else if index == 2 {
                              
                              cell.movieImageView.image = API.getApi().comedyTV[indexPath.row].image
                                  
                              
                                             
                              cell.movieURL = API.getApi().comedyTV[indexPath.row].url
                              
                              
                              
                          }else if index == 3 {
                              
                              cell.movieImageView.image = API.getApi().dramaTV[indexPath.row].image
                                             
                              cell.movieURL = API.getApi().dramaTV[indexPath.row].url
                                         
                          }else if index == 4 {
                              
                              
                              cell.movieImageView.image = API.getApi().thrillerTV[indexPath.row].image
                                             
                              cell.movieURL = API.getApi().thrillerTV[indexPath.row].url
                              
                          
                          }else if index == 5 {
                              
                              cell.movieImageView.image = API.getApi().actionAndAdventureTV  [indexPath.row].image
                                             
                              cell.movieURL = API.getApi().actionAndAdventureTV[indexPath.row].url
                              
                              
                          }
                          
                    
                return cell
                
                
            }else {
                
          
                
                
                let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell
                    
                    let index = indexPathForCell!.row
                          
                        
                          cell.movieImageView.layer.cornerRadius = 8
                          
                          if index == 1 {
                            
                            
                            cell.movieImageView.image = API.getApi().animatedMovies[indexPath.row].image
                                                                        
                                                         cell.movieURL = API.getApi().animatedMovies[indexPath.row].url
                              
                              
                            
                              
                        }
                          else if index == 2 {
                              
                            
                            
                            cell.movieImageView.image = API.getApi().kidsAndFamilyMovies[indexPath.row].image
                                
                            cell.movieURL = API.getApi().kidsAndFamilyMovies[indexPath.row].url
                         
                              
                              
                              
                          }else if index == 3 {
                            
                            
                            cell.movieImageView.image = API.getApi().tvAndMoviesForLittleKids[indexPath.row].image
                                                         
                                                     
                                                                    
                        cell.movieURL = API.getApi().tvAndMoviesForLittleKids[indexPath.row].url
                            
                              
                             
                                         
                          }else if index == 4 {
                              
                              
                            cell.movieImageView.image = API.getApi().tvAndMoviesForLittleKids[indexPath.row].image
                                                                          
                                                           cell.movieURL = API.getApi().tvAndMoviesForLittleKids[indexPath.row].url
                              
                          
                          }else if index == 5 {
                              
                              cell.movieImageView.image = API.getApi().kidsAndFamilyTV   [indexPath.row].image
                                             
                              cell.movieURL = API.getApi().kidsAndFamilyTV[indexPath.row].url
                              
                              
                          }
                          
                    
                return cell
                

            }
            
     
        }
          
        
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            
           
            return CGSize (width: 155 ,height : 130)
                

        }

        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 10.0
        }

        func collectionView(_ collectionView: UICollectionView, layout
            collectionViewLayout: UICollectionViewLayout,
                            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10.0
        }
        
        
        
        
        
    }
