//
//  CollectionViewCell.swift
//  Prime Video
//
//  Created by Alessandro Barruffo on 12/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//

import UIKit
import NotificationCenter


class CollectionViewCell: UICollectionViewCell {
    
    
    
    
    
    var movieURL : String?
    
    @IBOutlet weak var movieImageView: UIImageView!
    
    
    @IBOutlet weak var movieButton: UIButton!
    
    
    
    override func awakeFromNib() {
        
        
           
       }
    
    
    @IBAction func buttonTapped(_ sender: Any) {
        
        
        let name = Notification.Name("buttonTapped")
        
        
        
        NotificationCenter.default.post(name: name, object: movieURL!)
        
       
        
        print("Notification sent")
        
         print(movieURL!)
        
        
        
    }
    
}
