//
//  FirstTableViewCell.swift
//  Prime Video
//
//  Created by Alessandro Barruffo on 11/12/2019.
//  Copyright © 2019 Alessandro Barruffo. All rights reserved.
//

import UIKit
import AVKit

class FirstTableViewCell: UITableViewCell, UIScrollViewDelegate {

    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var segmentIndex  : Int?
    
    var contentWidth : CGFloat  = 0.0
    
    
    
    
    override func awakeFromNib() {
        
        
        super.awakeFromNib()
        
      
        scrollView.delegate = self
        
        
        let nameSegControlChanged = Notification.Name("segmentedControlChanged")
                       
      NotificationCenter.default.addObserver(self, selector: #selector(firstRowReloadData), name: nameSegControlChanged, object: segmentIndex)
                  
        
     
      for i in 0...2 {
        
         showFirstRow(imageName: "Image\(i+22)", imageIndex: i)
    
        }
 
        
      scrollView.contentSize = CGSize(width: contentWidth, height: self.frame.height)
        
        contentWidth = 0
    
    }
    

    
    @objc func firstRowReloadData(notification : Notification) {
               
            
          segmentIndex = notification.object as! Int?
        
        
        if segmentIndex == 0
        {
            
            for i in 0...2 {
                
                showFirstRow(imageName: "Image\(i+22)", imageIndex: i)
            }
            
           scrollView.contentSize = CGSize(width: contentWidth, height: self.frame.height)
            
            
            contentWidth = 0
            
            
                           
            
        }else if segmentIndex == 1 {
            
            
            for i in 0...2 {
                
                
                showFirstRow(imageName: "Image\(i)", imageIndex: i)
                
                
                      
            }
           scrollView.contentSize = CGSize(width: contentWidth, height: self.frame.height)
            
            contentWidth = 0
            
        }else {
            
            for i in 0...2 {
                
                showFirstRow(imageName: "Image\(i+5)", imageIndex: i)
                
            }
            
           scrollView.contentSize = CGSize(width: contentWidth, height: self.frame.height)
                           
            contentWidth = 0
            
        }
        
}
    
    
    func showFirstRow(imageName : String,imageIndex : Int ){
        
        

            let imageToDisplay = UIImage(named : imageName)
                
            let imageView = UIImageView(image: imageToDisplay)
               

            scrollView.frame = CGRect(x: 0 , y: 0 , width: self.frame.width , height: self.frame.height)
                
            scrollView.addSubview(imageView)
                
                
            let xCoordinate = self.frame.minX + self.frame.width * CGFloat(imageIndex)
                
                
            contentWidth += self.frame.width
                    

            imageView.frame = CGRect(x: xCoordinate, y: 0, width: 420, height: 195)
        
        }
        
        
        
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        
         super.setSelected(selected, animated: animated)

       
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
      pageControl.currentPage = Int(scrollView.contentOffset.x/CGFloat(414))
        
        
        
    }
    
   
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        
        
        let name = Notification.Name("buttonTapped")
                       
        var notification = Notification(name: name)
        
        
        if segmentIndex == 0 {
            
            
            notification.object = API.getApi().topTV[pageControl.currentPage].getUrl()
            
        
        }else if segmentIndex == 1 {
            
         
              if pageControl.currentPage == 0 {
                              
             notification.object = "https://lnx.farmaciaeuropa.it/ada/video7.mov"
                              
          }else {
                              
               notification.object = "https://lnx.farmaciaeuropa.it/ada/video\(pageControl.currentPage+1).mov"
                                              
            }
            
        }else {
            
        
            notification.object = API.getApi().animatedMovies[pageControl.currentPage].getUrl()
            
            
        }

                        
            NotificationCenter.default.post(notification)
                    
 
            
            
    }
    
    
    
       
    

}
